MongoDB Relationship Test
=========================

This project is a simple Spring Boot application demonstrating relationships between collections using MongoDB. The project models a system where users can rent vehicles, with the following collections:

-   `users`
-   `vehicles`
-   `rentals` (embedded in users)

Table of Contents
-----------------

1.  [Project Structure](#project-structure)
2.  [Relationship Explanation](#relationship-explanation)
3.  [Running the Project with Docker](#running-the-project-with-docker)
4.  [Endpoints](#endpoints)

Project Structure
-----------------

The project has the following structure:

-   `User`: Represents a user with unique ID, username, email, and a list of rentals.
-   `Vehicle`: Represents a vehicle with unique ID, make, model, and registration number.
-   `Rental`: Represents a rental with a reference to a vehicle, rental date, and return date.

Relationship Explanation
------------------------

### User and Rental

-   Each `User` document contains a list of embedded `Rental` documents.
-   A `Rental` document references a `Vehicle` by its `vehicleId`.

### Vehicle

-   The `Vehicle` document contains information about each vehicle available for rent.

### Emulating Joins

In MongoDB, joins are emulated by referencing IDs between collections and embedding documents:

-   **Embedded Documents**: The `Rental` documents are embedded within the `User` documents. This allows for efficient access to a user's rentals.
-   **Referenced Documents**: The `Rental` documents reference the `Vehicle` documents by their `vehicleId`. This allows for separate management of vehicles and rentals.

Running the Project with Docker
-------------------------------

### Prerequisites

-   Docker
-   Docker Compose

### Steps

1.  **Build and Run Containers:**

    `docker-compose up --build`

2.  **Access the Application:** The application will be available at `http://localhost:8080`.

3.  **MongoDB Access:** MongoDB will be running at `mongodb://localhost:27017`.

Endpoints
---------

### User Endpoints

-   **Create User:** `POST /api/users`

    -   Request Body:

        `{
        "username": "john_doe",
        "email": "john.doe@example.com",
        "rentals": []
        }`

-   **Get All Users:** `GET /api/users`

-   **Get User by ID:** `GET /api/users/{id}`

-   **Delete User:** `DELETE /api/users/{id}`

-   **Add Rental to User:** `POST /api/users/{userId}/rentals`

    -   Request Body:
        `{
        "vehicleId": "vehicle_id_here",
        "rentalDate": "2024-07-01T10:00:00",
        "returnDate": "2024-07-10T10:00:00"
        }`

### Vehicle Endpoints

-   **Create Vehicle:** `POST /api/vehicles`

    -   Request Body:

        `{
        "make": "Toyota",
        "model": "Corolla",
        "registrationNumber": "XYZ123"
        }`

-   **Get All Vehicles:** `GET /api/vehicles`

-   **Get Vehicle by ID:** `GET /api/vehicles/{id}`

-   **Delete Vehicle:** `DELETE /api/vehicles/{id}`

Data Initialization
-------------------

The `DataInitializer` class populates the database with initial data: 3 vehicles, 2 users, and 5 rentals for the year 2024.

By following these instructions, you can set up and run the project, as well as understand how the relationships between users, vehicles, and rentals are managed and queried in a MongoDB environment.