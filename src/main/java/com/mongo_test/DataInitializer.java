package com.mongo_test;

import com.mongo_test.models.Rental;
import com.mongo_test.models.User;
import com.mongo_test.models.Vehicle;
import com.mongo_test.repositories.UserRepository;
import com.mongo_test.repositories.VehicleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;

@Component
@Slf4j
@RequiredArgsConstructor
public class DataInitializer implements CommandLineRunner {

    private final UserRepository userRepository;
    private final VehicleRepository vehicleRepository;

    @Override
    public void run(String... args) throws Exception {
        userRepository.deleteAll();
        vehicleRepository.deleteAll();

        Vehicle vehicle1 = new Vehicle();
        vehicle1.setMake("Toyota");
        vehicle1.setModel("Corolla");
        vehicle1.setRegistrationNumber("XYZ123");

        Vehicle vehicle2 = new Vehicle();
        vehicle2.setMake("Honda");
        vehicle2.setModel("Civic");
        vehicle2.setRegistrationNumber("ABC456");

        Vehicle vehicle3 = new Vehicle();
        vehicle3.setMake("Ford");
        vehicle3.setModel("Mustang");
        vehicle3.setRegistrationNumber("LMN789");

        vehicleRepository.saveAll(Arrays.asList(vehicle1, vehicle2, vehicle3));

        User user1 = new User();
        user1.setUsername("john_doe");
        user1.setEmail("john.doe@example.com");

        User user2 = new User();
        user2.setUsername("jane_smith");
        user2.setEmail("jane.smith@example.com");

        Rental rental1 = new Rental();
        rental1.setVehicleId(vehicle1.getId());
        rental1.setRentalDate(LocalDateTime.of(2024, 1, 10, 10, 0));
        rental1.setReturnDate(LocalDateTime.of(2024, 1, 20, 10, 0));

        Rental rental2 = new Rental();
        rental2.setVehicleId(vehicle2.getId());
        rental2.setRentalDate(LocalDateTime.of(2024, 2, 15, 10, 0));
        rental2.setReturnDate(LocalDateTime.of(2024, 2, 25, 10, 0));

        Rental rental3 = new Rental();
        rental3.setVehicleId(vehicle3.getId());
        rental3.setRentalDate(LocalDateTime.of(2024, 3, 20, 10, 0));
        rental3.setReturnDate(LocalDateTime.of(2024, 3, 30, 10, 0));

        Rental rental4 = new Rental();
        rental4.setVehicleId(vehicle1.getId());
        rental4.setRentalDate(LocalDateTime.of(2024, 4, 10, 10, 0));
        rental4.setReturnDate(LocalDateTime.of(2024, 4, 20, 10, 0));

        Rental rental5 = new Rental();
        rental5.setVehicleId(vehicle2.getId());
        rental5.setRentalDate(LocalDateTime.of(2024, 5, 15, 10, 0));
        rental5.setReturnDate(LocalDateTime.of(2024, 5, 25, 10, 0));

        user1.setRentals(Arrays.asList(rental1, rental2, rental3));
        user2.setRentals(Arrays.asList(rental4, rental5));

        userRepository.saveAll(Arrays.asList(user1, user2));

        log.info("Data initialization complete.");
    }
}