package com.mongo_test.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Rental {
    private String vehicleId;
    private LocalDateTime rentalDate;
    private LocalDateTime returnDate;
}