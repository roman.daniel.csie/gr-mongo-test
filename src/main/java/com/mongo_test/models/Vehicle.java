package com.mongo_test.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@NoArgsConstructor
@Document(collection = "vehicles")
public class Vehicle {
    @Id
    private String id = UUID.randomUUID().toString();

    private String make;
    private String model;
    private String registrationNumber;
}