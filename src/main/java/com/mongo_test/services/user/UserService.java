package com.mongo_test.services.user;

import com.mongo_test.models.Rental;
import com.mongo_test.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    Optional<User> getUserById(String id);

    User saveUser(User user);

    void deleteUser(String id);

    User addRental(String userId, Rental rental);
}