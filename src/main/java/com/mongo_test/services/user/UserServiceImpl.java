package com.mongo_test.services.user;

import com.mongo_test.models.Rental;
import com.mongo_test.models.User;
import com.mongo_test.models.Vehicle;
import com.mongo_test.repositories.UserRepository;
import com.mongo_test.repositories.VehicleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final VehicleRepository vehicleRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> getUserById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public User addRental(String userId, Rental rental) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
        Vehicle vehicle = vehicleRepository.findById(rental.getVehicleId()).orElseThrow(() -> new RuntimeException("Vehicle not found"));

        user.getRentals().add(rental);
        return userRepository.save(user);
    }
}