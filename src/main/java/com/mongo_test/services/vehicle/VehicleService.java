package com.mongo_test.services.vehicle;

import com.mongo_test.models.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleService {
    List<Vehicle> getAllVehicles();

    Optional<Vehicle> getVehicleById(String id);

    Vehicle saveVehicle(Vehicle vehicle);

    void deleteVehicle(String id);
}